<?php

namespace App;

use App\Exercise;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $guarded = ['id'];

    public function exercises()
    {
        return $this->hasMany(Exercise::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
