<?php

namespace App\Http\Controllers;

use App\Level;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $levels = Level::active()->get();
        return view('home', compact('levels'));
    }

    public function start()
    {
        return view('start');
    }
}
