<?php

namespace App\Http\Controllers;

use App\Level;
use App\Http\Requests\RequestLevel;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function index()
    {
        $levels = Level::with('exercises')->get();
        return view('level.index', compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function create()
    {
        return view('level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RequestLevel  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestLevel $request)
    {
        $level = new Level($request->all());
        $level->save();
        return redirect()->route('level.index')->with(['msg-success' => 'Niveau créé.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Support\Facades\View
     */
    public function show(Level $level)
    {
        $exercises = $level->exercises;
        return view('level.show', compact('exercises'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Level $level)
    {
        return view('level.edit', compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RequestLevel  $request
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(RequestLevel $request, Level $level)
    {
        $request['active'] = isset($request['active']) ?? false;
        $level->update($request->all());
        return redirect()->route('level.index')->with(['msg-success' => 'Niveau mis à jour.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        $level->delete();
        return redirect()->route('level.index')->with(['msg-success' => 'Niveau supprimé.']);
    }
}
