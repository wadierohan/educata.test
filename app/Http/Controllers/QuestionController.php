<?php

namespace App\Http\Controllers;

use App\Level;
use App\Question;
use App\Http\Requests\RequestQuestion;
use App\Exercise;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Level $level, Exercise $exercise)
    {
        $questions = Question::with('exercise')->orderBy('exercise_id', 'desc')->orderBy('order')->get();
        return view('question.index', compact('level', 'exercise', 'questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Level $level, Exercise $exercise)
    {
        $levels = Level::with('exercises')->get();
        return view('question.create', compact('level', 'exercise', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RequestQuestion  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Level $level, Exercise $exercise, RequestQuestion $request)
    {
        $inputs = $request->all();
        $inputs['possible_answer'] = json_encode(explode(' ', $inputs['possible_answer']));
        //$inputs['order'] = Question::where('exercise_id', $exercise->id)->max('order') + 1;
        $question = new Question($inputs);
        $question->save();
        return redirect()->route('question.index', [$level->id, $exercise->id])->with(['msg-success' => 'Question créé.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Support\Facades\View
     */
    public function show(Level $level, Exercise $exercise, Question $question)
    {
        return view('question.edit', compact('level', 'exercise', 'question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Level $level, Exercise $exercise, Question $question)
    {
        $levels = Level::with('exercises')->get();
        return view('question.edit', compact('level', 'exercise', 'question', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RequestQuestion  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Level $level, Exercise $exercise, RequestQuestion $request, Question $question)
    {
        $inputs = $request->all();
        $inputs['possible_answer'] = json_encode(explode(' ', $inputs['possible_answer']));
        $question->update($inputs);
        return redirect()->route('question.index', [$level->id, $exercise->id])->with(['msg-success' => 'Question mis à jour.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level, Exercise $exercise, Question $question)
    {
        $question->delete();
        return redirect()->route('question.index', [$level->id, $exercise->id])->with(['msg-success' => 'Question supprimé.']);
    }

    public function apiGetQuestion(Question $question)
    {
        return response()->json($question);
    }
}
