<?php

namespace App\Http\Controllers;

use App\Level;
use App\Exercise;
use App\Question;
use App\Http\Requests\RequestExercise;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Level $level)
    {
        $exercises = Exercise::with(['questions', 'level'])->get();
        return view('exercise.index', compact('level', 'exercises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Level $level)
    {
        $levels = Level::all();
        return view('exercise.create', compact('level', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RequestExercise  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Level $level, RequestExercise $request)
    {
        $values = $request->all();
        $values['image'] = $request->hasFile('image') ? $request->file('image')->store('exercises', 'public') : null;
        $exercise = new Exercise($values);
        $exercise->save();
        return redirect()->route('exercise.index', $level->id)->with(['msg-success' => 'Exercise créé.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Support\Facades\View
     */
    public function show(Level $level, Exercise $exercise)
    {
        return view('exercise.show', compact('exercise', 'level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Level $level, Exercise $exercise)
    {
        $levels = Level::all();
        return view('exercise.edit', compact('level', 'levels', 'exercise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RequestExercise  $request
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function update(Level $level, RequestExercise $request, Exercise $exercise)
    {
        $values = $request->all();
        if ($request->hasFile('img')) {
            $values['image'] = $request->file('img')->store('exercises', 'public');
        }
        $values['active'] = isset($request['active']) ?? false;
        $values['chrono'] = isset($request['chrono']) ?? false;
        $exercise->update($values);
        return redirect()->route('exercise.index', $level->id)->with(['msg-success' => 'Exercise mis à jour.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level, Exercise $exercise)
    {
        $exercise->delete();
        return redirect()->route('exercise.index', $level->id)->with(['msg-success' => 'Exercise supprimé.']);
    }

    public function apiGetQuestionCount(Exercise $exercise)
    {
        return response()->json($exercise->questions()->count());
    }

    public function apiGetQuestion(Exercise $exercise, $order)
    {
        $question = $exercise->questions()->byOrder($order)->first();
        return response()->json($question);
    }
}
