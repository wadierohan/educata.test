<?php

namespace App;

use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\Model;

class Question extends Model implements Sortable
{
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    protected $guarded = ['id'];

    public function getPossibleAnswerAttribute($value)
    {
        return json_decode($value);
    }

    public function getPossibleAnswerStringAttribute()
    {
        return implode(' ', $this->possible_answer);
    }

    public function getCorrectAnswerAttribute($value)
    {
        return json_decode($value);
    }

    public function getCorrectAnswerStringAttribute()
    {
        $string = [];
        foreach ($this->correct_answer as $row) {
            $string[] = $row->el;
        }
        return implode(' ', $string);
    }

    public function exercise()
    {
        return $this->belongsTo(Exercise::class);
    }

    public function scopeByOrder($query, $order)
    {
        return $query->where('order', $order);
    }
}
