<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);
Route::get('/', 'HomeController@index')->name('home');
Route::get('/start', 'HomeController@start')->name('start');
Route::resource('level', 'LevelController', ['only' => ['show']]);
Route::resource('exercise', 'ExerciseController', ['only' => ['show']]);

Route::prefix('admin')->middleware('auth:web')->group(function () {
    Route::resource('level', 'LevelController', ['except' => ['show']]);
    Route::prefix('level/{level}')->group(function () {
        Route::resource('exercise', 'ExerciseController', ['except' => ['show']]);
        Route::prefix('exercise/{exercise}')->group(function () {
            Route::resource('question', 'QuestionController');
        });
    });
});
