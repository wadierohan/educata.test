@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        @foreach ($exercises as $row)
            <div class="col-md-3">
                <div class="card">
                    @if($row->image)
                    <img class="card-img-top" src="{{asset('storage/'.$row->image)}}">
                    @endif
                    <div class="card-body">
                        <h4 class="card-title">{{ $row->name }}</h4>
                        <p class="card-text">{{ $row->description }}</p>
                        <a href="{{route('exercise.show', $row->id)}}" class="btn btn-primary">Go!</a>
                    </div>
                </div>
            </div>
        @endforeach
        
    </div>
</div>
@endsection