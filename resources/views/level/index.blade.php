@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-2">
            <a class="btn btn-primary float-right" href="{{route('level.create')}}" role="button">Nouveau</a>
        </div>
        <div class="col-md-12">
            <table class="table table-light table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Nom</th>
                        <th>Active</th>
                        <th>Nombre exercises</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($levels as $level)
                    <tr>
                        <td scope="row">{{$level->name}}</td>
                        <td>{{$level->active ? 'Oui' : 'Non'}}</td>
                        <td>{{$level->exercises->count()}}</td>
                        <td class="text-right">
                            <a href="{{ route('exercise.index', $level->id) }}"><i class="fab fa-leanpub"></i></a>
                            <a class="edit-model" href="{{ route('level.edit', $level->id) }}"><i class="fas fa-edit"></i></a>
                            <form class="delete-model-form" method="POST" action="{{ route('level.destroy', $level->id) }}">
                                @csrf
                                @method('DELETE')
                                <i onclick="deleteModel(event)" class="fas fa-trash-alt delete-model"></i>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection