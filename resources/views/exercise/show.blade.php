@extends('layouts.app')

@section('content')
<div class="container">
    <question-component level_id="{{$exercise->level->id}}" exercise_id="{{$exercise->id}}"></question-component>
</div>
@endsection