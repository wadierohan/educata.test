@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-2">
            <a class="btn btn-primary float-right" href="{{route('exercise.create', [$level->id])}}" role="button">Nouveau</a>
        </div>
        <div class="col-md-12">
            <table class="table table-light table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Image</th>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Active</th>
                        <th>Nombre questions</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($exercises as $exercise)
                    <tr>
                        <td class="align-middle"><img style="width:150px;" src="{{asset('storage/'.$exercise->image)}}" class="img-thumbnail" alt="{{$exercise->name}}"></td>
                        <td class="align-middle">{{$exercise->name}}</td>
                        <td class="align-middle"><pre class="mb-0">{{$exercise->description}}</pre></td>
                        <td class="align-middle">{{$exercise->active ? 'Oui' : 'Non'}}</td>
                        <td class="align-middle">{{$exercise->questions->count()}}</td>
                        <td class="text-right align-middle">
                            <a href="{{ route('question.index', [$level->id, $exercise->id]) }}"><i class="fas fa-list-alt"></i></a>
                            <a class="edit-model" href="{{ route('exercise.edit', [$level->id, $exercise->id]) }}"><i class="fas fa-edit"></i></a>
                            <form class="delete-model-form" method="POST" action="{{ route('exercise.destroy', [$level->id, $exercise->id]) }}">
                                @csrf
                                @method('DELETE')
                                <i onclick="deleteModel(event)" class="fas fa-trash-alt delete-model"></i>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection