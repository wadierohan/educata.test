@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('exercise.store', [$level->id]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input class="form-control" type="file" name="image" id="image">
                        </div>
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="level_id">Niveau</label>
                            <select class="form-control" name="level_id" id="level_id">
                                <option value="">----</option>
                                @foreach($levels as $row)
                                    <option value="{{ $row->id }}" {{ $row->id === $level->id ? 'selected="selected"' : '' }} >{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="active" id="active" value="1" checked>
                                Active
                            </label>
                        </div>
                        
                        <button type="submit" class="btn btn-primary float-right">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection