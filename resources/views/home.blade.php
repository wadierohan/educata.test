@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($levels as $level)
        <div class="col-md-4">
            <div class="card level">
                <div class="card-body">
                    <p>{{ $level->name }}</p>
                    <a href="{{route('level.show', $level->id)}}" class="btn btn-primary">Choisir</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
