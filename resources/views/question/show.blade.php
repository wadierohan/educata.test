@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('storage/'.$exercise->image)}}" class="img-thumbnail" alt="{{$exercise->name}}">
                    <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $exercise->name }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description">{{ $exercise->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="level_id">Niveau</label>
                        <select class="form-control" name="level_id" id="level_id">
                            <option value="">----</option>
                            @foreach($levels as $level)
                                <option value="{{ $level->id }}" {{ $exercise->level_id === $level->id ? 'selected="selected"' : '' }} >{{ $level->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="chrono" id="chrono" value="1" {{ $exercise->chrono ? 'checked="checked"' : '' }} >
                            Chrono
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="active" id="active" value="1" {{ $exercise->active ? 'checked="checked"' : '' }} >
                            Active
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection