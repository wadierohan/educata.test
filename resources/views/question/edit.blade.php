@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="question-form" method="POST" action="{{ route('question.update', [$level->id, $exercise->id, $question->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label for="question">Question</label>
                            <textarea class="form-control" name="question" id="question">{!! $question->question !!}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exercise_id">Exercise</label>
                            <select class="form-control" name="exercise_id" id="exercise_id">
                                <option value="">----</option>
                                @foreach($levels as $level)
                                    <optgroup label="{{ $level->name }}">
                                        @foreach ($level->exercises as $row)
                                            <option value="{{ $row->id }}" {{ $question->exercise_id === $row->id ? 'selected="selected"' : ''}}>{{ $row->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="correct_answer">La bonne réponse</label>
                            <input type="text" class="form-control" id="correct_answer" name="correct_answer" value="{{ $question->correct_answer_string }}">
                            <div class="card">
                                <div class="card-body" id="answer">
                                    @foreach($question->correct_answer as $tag)
                                    <span data-answer="{{ $tag->el }}" class="badge {{ $tag->inserted ? 'badge-success' : 'badge-secondary' }}">{{ $tag->el }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="possible_answer">Les réponses possibles</label>
                            <input type="text" class="form-control" id="possible_answer" name="possible_answer" value="{{ $question->possible_answer_string }}">
                            <div class="card">
                                <div class="card-body" id="possible-answer">
                                    @foreach($question->possible_answer as $tag)
                                    <span class="badge badge-secondary">{{ $tag }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        (function(){
            $('#answer').on('click', '.badge', function(){
                $(this).toggleClass('badge-success badge-secondary')
            })
            $('#correct_answer').change(function(){
                $('#answer').html('')
                const correctAnswer = $(this).val().split(' ')
                $.each(correctAnswer, function(i, v){
                    $('#answer').append('<span data-answer="'+v+'" class="badge badge-secondary">'+v+'</span>')
                })
            })
            $('#possible_answer').change(function(){
                $('#possible-answer').html('')
                const possibleAnswer = $(this).val().split(' ')
                $.each(possibleAnswer, function(i, v){
                    $('#possible-answer').append('<span class="badge badge-secondary">'+v+'</span>')
                })
            })
            $('#question-form').submit(function(){
                let data = []
                $('#answer .badge').each(function(i, el){
                    data.push({
                        el: $(el).data('answer'),
                        inserted: $(el).hasClass('badge-success')
                    })
                })
                $('#correct_answer').val(JSON.stringify(data))
            })
        })()
    </script>
@endsection