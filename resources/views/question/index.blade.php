@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-2">
            <a class="btn btn-primary float-right" href="{{route('question.create', [$level->id, $exercise->id])}}" role="button">Nouveau</a>
        </div>
        <div class="col-md-12">
            <table class="table table-light table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Order</th>
                        <th>Question</th>
                        <th>Exercise</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($questions as $question)
                    <tr>
                        <td class="align-middle">{{$question->order}}</td>
                        <td class="align-middle"><pre class="mb-0">{!!$question->question!!}</pre></td>
                        <td class="align-middle">{{$question->exercise ? $question->exercise->name : ''}}</td>
                        <td class="text-right align-middle">
                            <a class="edit-model" href="{{ route('question.edit', [$level->id, $exercise->id, $question->id]) }}"><i class="fas fa-edit"></i></a>
                            <form class="delete-model-form" method="POST" action="{{ route('question.destroy', [$level->id, $exercise->id, $question->id]) }}">
                                @csrf
                                @method('DELETE')
                                <i onclick="deleteModel(event)" class="fas fa-trash-alt delete-model"></i>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection