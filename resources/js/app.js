
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import swal from 'sweetalert';
require('select2/dist/js/select2')
require('jquery-ui')
import toastr from 'toastr'
import vuetoastr from 'vue-toastr'
import Vue from 'vue';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('question-component', require('./components/QuestionComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(vuetoastr, { "defaultPosition": "toast-top-center" });

const app = new Vue({
    el: '#app'
});

toastr.options = {
    "positionClass": "toast-bottom-right",
}

window.deleteModel = (event) => {
    const form = event.target.closest('form')
    swal({
        text: "Êtes vous sûr de vouloir supprimer cet elément?",
        icon: "warning",
        dangerMode: true,
        buttons:
        {
            cancel: {
              text: "Non",
              value: null,
              visible: true,
              className: "",
              closeModal: true,
            },
            confirm: {
              text: "Oui",
              value: true,
              visible: true,
              className: "red-bg",
              closeModal: true
            }
        },
    }).then((isConfirm) => {
        if (isConfirm) {
            form.submit()
        }
    })
}

window.$(".select2-tag").select2({
    tags: true,
    width: 'style',
    tokenSeparators: [',', ' ']
}).on(
    'select2:select, select2:close',(
        function(){
            $(this).focus();
        }
    )
);
